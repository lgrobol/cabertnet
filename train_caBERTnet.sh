#! /bin/bash

zeldarose-transformer local/CaBeRnetFRanc_seg.txt \
--out-dir "$SCRATCH/CaBERTnet/$SLURM_JOB_ID" \
--device-batch-size 16 \
--accelerator ddp \
--n-gpus 4 \
--n-workers 4 \
--tokenizer "local/caBERTnet_tokenizer" \
--model-config roberta-base \
--max-epochs 64 \
--max-steps 110000 \
--config mlm-roBERTa.toml \
--model-name caBERTnet-base \
--save-period 1