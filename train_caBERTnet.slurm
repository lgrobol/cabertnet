#! /bin/bash

#SBATCH --qos="qos_gpu-t3"
#SBATCH --constraint v100-32g
#SBATCH --nodes="32" # Combien de nœuds
#SBATCH --ntasks-per-node="4" # Une tâche par GPU
#SBATCH --gres="gpu:4" # nombre de GPU par nœud
#SBATCH --cpus-per-task="10" # nombre de coeurs à réserver par tâche
#SBATCH --hint="nomultithread" # on réserve des coeurs physiques et non logiques
#SBATCH --time="20:00:00" # temps d'exécution maximum demande (HH:MM:SS)
set -x

module purge

# On ne devrait pas avoir besoin de charger CUDA/CuDNN puisque vendorisés dans torch
# À tester: forcer la version CUDA 11 de torch dans requirements…
module load gcc/9.1.0
module load cuda/10.2
module load nccl/2.9.6-1-cuda
module load cudnn/8.0.4.30-cuda-10.2
module load intel-mkl/2020.4
module load magma/2.5.4-cuda
module load openmpi/4.1.1-cuda

source .venv/bin/activate

export NCCL_DEBUG=INFO
export PYTHONFAULTHANDLER=1
export PYTHONUNBUFFERED=1
export HF_DATASETS_CACHE="$SCRATCH/cache/hf-datasets"
export TRANSFORMERS_OFFLINE=1

mkdir "slurm-$SLURM_JOBID"
srun -o "slurm-$SLURM_JOBID/out_%t" "train_caBERTnet.sh"