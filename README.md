caBERTnet
=========

Setup

1. Clone the repo

   ```sh
   git clone https://gitlab.huma-num.fr/lgrobol/cabertnet.git
   cd cabertnet
   ```

2. Get `CaBeRnet.zip`
3. Extract the data in `local`

   ```sh
   mkdir local
   unzip -j -o "{path_to}/CaBeRnet.zip" "CaBeRnet/CaBeRnetFRanc_seg.txt" -d "local/"
   ```

4. Prepare the virtualenv in which we will run and train the tokenizer (running on the preprocessing partition)

   ```sh
   python3 -m venv .venv
   source .venv/bin/activate
   pip install -U pip
   pip install -U -r requirements.txt
   transformers-cli download roberta-base
   TOKENIZERS_PARALLELISM=true srun --account={myaccount@gpu} --time="2:00:00" --pty --ntasks=1 --cpus-per-task=4 --partition=prepost  --hint=nomultithread zeldarose-tokenizer --vocab-size 32768 --out-path local/ --model-name "caBERTnet_tokenizer" local/CaBeRnetFRanc_seg.txt
   deactivate
   ```

Once that is done, you can run the experiment in

```console
sbatch --account={myaccount@gpu} train_caBERTnet.slurm
```

The outputs will be in `$SCRATCH/caBERTnet` in a subdir named with the SLURM job id.

**Note** Remember to set `HF_DATASETS_CACHE` to something in `$SCRATCH` to avoid filling your home
given jean-zay quotas.